﻿using Chromely;
using Chromely.Core;
using Chromely.Core.Configuration;
using EsmoShell.Application.Window.Commands.DisableSystemMenu;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;

namespace EsmoShell
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            var config = DefaultConfiguration.CreateForRuntimePlatform();
            config.StartUrl = "local://dist/index.html";
            config.WindowOptions.FramelessOption.UseWebkitAppRegions = true;

            var chromely = new ChromelyApp();
            AppBuilder
            .Create()
            .UseConfig<DefaultConfiguration>(config)
            .UseApp<ChromelyApp>(chromely)
            .Build()
            .Run(args);
        }
    }
    public class ChromelyApp : ChromelyBasicApp
    {
        private static ChromelyApp Instance { get; set; }

        public static ChromelyApp GetInstance() => Instance;
        /*
        static IChromelyConfiguration _config = DefaultConfiguration.CreateForRuntimePlatform();
        public static IChromelyConfiguration GetChromelyConfiguration()
        {
            return _config;
        }*/
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            services.AddLogging(configure => configure.AddConsole());
            services.AddLogging(configure => configure.AddFile("Logs/serilog-{Date}.txt"));
            /*
                        // Optional - adding custom handler
                        services.AddSingleton<CefDragHandler, CustomDragHandler>();
                        */

            /*
            // Optional- using config section to register IChromelyConfiguration
            // This just shows how it can be used, developers can use custom classes to override this approach
            //
            var builder = new ConfigurationBuilder()
                    .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");

            var configuration = builder.Build();
            var config = DefaultConfiguration.CreateFromConfigSection(configuration);
            services.AddSingleton<IChromelyConfiguration>(config);
            */

            /* Optional
            var options = new JsonSerializerOptions();
            options.ReadCommentHandling = JsonCommentHandling.Skip;
            options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
            options.AllowTrailingCommas = true;
            services.AddSingleton<JsonSerializerOptions>(options);
            */

            //_config.WindowOptions.DisableMinMaximizeControls = true;
            //config.WindowOptions.DisableResizing = true;

            //_config.WindowOptions.WindowState = Chromely.Core.Host.WindowState.Maximize;
            //_config.WindowOptions.FramelessOption.UseWebkitAppRegions = true;
            //_config.WindowOptions.WindowFrameless = true;
            //services.AddSingleton(_config);
            //config.WindowOptions.WindowState = Chromely.Core.Host.WindowState.Maximize;
            // other service configuration can be placed here
            RegisterControllerAssembly(services, typeof(ChromelyApp).Assembly);
        }
        public override void Initialize(IServiceProvider serviceProvider)
        {
            base.Initialize(serviceProvider);
            Instance = this;
            new DisableSystemMenuWindowCommandHandler().Handle();
        }
        /*
        public override void Initialize(IServiceProvider serviceProvider)
        {
            base.Initialize(serviceProvider);
            _config.StartUrl = "local://dist/index.html";
            //_config.WindowOptions.WindowState = Chromely.Core.Host.WindowState.Normal;
            //_config.WindowOptions.WindowState = Chromely.Core.Host.WindowState.Maximize;
            //_config.WindowOptions.WindowFrameless = true;
            var process = Process.GetCurrentProcess();
            Task.Run(() =>
            {
                Task.Delay(5000).Wait();
                //_config.WindowOptions.WindowFrameless = true;
                //_config.WindowOptions.WindowState = Chromely.Core.Host.WindowState.Maximize;
                //_config.JavaScriptExecutor?.ExecuteScript("window.location.replace(\"res://app/index.html\");");
                _config.JavaScriptExecutor?.ExecuteScript("document.getElementById(\"random\").innerHTML = 'hi mazafaka'");
                //WindowPlacement.MaximizeWindow(process.MainWindowHandle);
                //WindowPlacement.MaximizeHandleToWorkArea(process.MainWindowHandle);
            });

        }*/
    }
}
