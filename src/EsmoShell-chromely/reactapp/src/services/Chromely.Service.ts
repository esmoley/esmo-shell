function messageRouterGet(url:string, parameters:any, callback:any, _self:any){
    var request = {
        "method": "GET",
        "url": url,
        "parameters": parameters,
        "postData": null
    };
    (window as any).cefQuery({
        request: JSON.stringify(request),
        onSuccess: (response:any) => {
            var jsonData = JSON.parse(response);
            if (jsonData.ReadyState == 4 && jsonData.Status == 200) {
                if(callback) callback(jsonData.Data, _self);
            } else {
                console.log("An error occurs during message routing. With ur:" + url + ". Response received:" + response);
            }
        },
        onFailure: (err:any, msg:any) => {
            console.log(err, msg);
        }
    });
}

function messageRouterPost(url:string, parameters:any, postData:any, callback:any, _self:any){
    var request = {
        "method": "POST",
        "url": url,
        "parameters": parameters,
        "postData": postData
    };
    (window as any).cefQuery({
        request: JSON.stringify(request),
        onSuccess: (response:any) => {
            var jsonData = JSON.parse(response);
            if (jsonData.ReadyState == 4 && jsonData.Status == 200) {
                if(callback) callback(jsonData.Data, _self);
            } else {
                console.log("An error occurs during message routing. With ur:" + url + ". Response received:" + response);
            }
        },
        onFailure: (err:any, msg:any) => {
            console.log(err, msg);
        }
    });
}

function openExternalUrl(url:string){
    var link = document.createElement('a');
        link.href = url;
        document.body.appendChild(link);
        link.click(); 
}
export {messageRouterGet, messageRouterPost, openExternalUrl}