﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace EsmoShell.Application.Window.Commands.DisableSystemMenu
{
    public class DisableSystemMenuWindowCommandHandler
    {
        private const int GWL_STYLE = -16; //WPF's Message code for Title Bar's Style 
        private const int WS_SYSMENU = 0x80000; //WPF's Message code for System Menu
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        public void Handle()
        {
            try
            {
                var pid = Process.GetCurrentProcess().Id;
                var handle = Process.GetCurrentProcess().Handle;
                var handle2 = Process.GetCurrentProcess().MainWindowHandle;
                var handle3 = Process.GetCurrentProcess().MainWindowTitle;
                Console.WriteLine(Process.GetCurrentProcess().Handle);
                Console.WriteLine(Process.GetCurrentProcess().MainWindowHandle);
                Console.WriteLine(Process.GetCurrentProcess().MainWindowTitle);

                //var hwnd = new WindowInteropHelper(ChromelyApp.GetInstance().
                SetWindowLong(handle, GWL_STYLE, GetWindowLong(handle, GWL_STYLE) & ~WS_SYSMENU);
            }
            catch {}
        }
    }
}
